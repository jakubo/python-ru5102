from setuptools import setup


setup(
    name='ru5102',
    version='0.1',
    url='https://gitlab.com/jakubo/python-ru5102',
    description=open('README').read(),
    packages=['ru5102'],
    license='MIT',
    long_description=open('README').read(),
    install_requires=['crcmod==1.7', 'pyserial==3.4'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
    ]
)

"""Simple library for CF-RU5102 UHF RFID reader.
Based on rust library found here: https://github.com/miek/ru5102/.

Usage:
    reader = Reader()
    reader.read_once()
"""
from time import sleep

import serial
from crcmod.predefined import Crc


__version__ = '0.1'


class Command:
    INVENTORY = 1

    def __init__(self, command, address, data=None):
        self.command = command
        self.address = address
        self.data = data or []

    def add_crc(self, packet):
        """Returns ``packet`` with appended CRC"""
        msg = bytearray(packet)
        crc16 = Crc('crc-16-mcrf4xx')
        crc16.update(msg)
        msg.append(crc16.crcValue & 0xFF)
        msg.append(crc16.crcValue >> 8)
        return msg

    def to_bytes(self):
        """Returns message ready for transmission"""
        pkt_len = len(self.data) + 4
        pkt = [pkt_len, self.address, self.command]
        pkt.extend(self.data)
        return self.add_crc(pkt)

    @classmethod
    def inventory(cls, address, data=None):
        return cls(cls.INVENTORY, address, data)


class Response:
    class Status:
        OK = 1
        NO_TAG = 0xFB

    def __init__(self, data):
        self.data = data

    def parse(self):
        size = len(self.data)
        payload = self.data[1:size-2]
        return {
            'address': payload[0],
            'command': payload[1],
            'status': payload[2],
            'data': payload[3:]
        }


class Reader(serial.Serial):
    PORT = '/dev/ttyUSB0'
    BAUD = 57600
    RESPONSE_WAIT = 50e-3

    def __init__(self, port=PORT):
        super().__init__(port, baudrate=self.BAUD)

    def read_once(self, reader_addr=0):
        """Wait for tag to show up and return it's data."""
        request = Command.inventory(reader_addr).to_bytes()
        while True:
           self.write(request)
           sleep(self.RESPONSE_WAIT)
           response = Response(self.read(self.in_waiting)).parse()
           if response['status'] == Response.Status.OK:
               return response['data'].hex()
